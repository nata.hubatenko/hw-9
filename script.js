const makeList = (cities, list) => {

	const listHtml = cities.map(elem => `<li>${elem}</li>`);
	list = document.createElement('ul');
	document.body.append(list);
	list.innerHTML = `${listHtml.join('')}`;
}
makeList(["Kyiv", "Bila Tserkva", "Kharkiv", "Odesa", "Lviv"]);


